//
//  UserCell.swift
//  KhaleefTask
//
//  Created by Muhammad Iqbal on 01/04/2020.
//  Copyright © 2020 Muhammad Iqbal. All rights reserved.
//

import UIKit

class UserCell: UITableViewCell {

    @IBOutlet weak var updateBtn: UIButton!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var UserEmailLbl: UILabel!
    @IBOutlet weak var userNameLbl: UILabel!

    
    var onUpdateClick : ((User) -> Void)?
    var onDeleteClick : ((User) -> Void)?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    

    
    var user : User? {
        didSet {
            
            guard let usr = user else {
                return
            }
            
            self.userNameLbl.text = usr.userName
            self.UserEmailLbl.text = usr.email
        }
    }

    @IBAction func actionButtonTapped(sender: UIButton) {
        
        guard let usr = user else {
            return
        }
        
        if sender.tag == 1 {
            // delete
            self.onDeleteClick?(usr)
        } else {
            // update
            self.onUpdateClick?(usr)
        }

   }
}
