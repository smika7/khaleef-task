//
//  RegisterUserVC.swift
//  KhaleefTask
//
//  Created by Muhammad Iqbal on 01/04/2020.
//  Copyright © 2020 Muhammad Iqbal. All rights reserved.
//

import UIKit

class RegisterUserVC: UIViewController {

    @IBOutlet weak var showUserBtn: UIButton!
    @IBOutlet weak var registerBtn: UIButton!
    @IBOutlet weak var userNameField: UITextField!
    @IBOutlet weak var lastNameField: UITextField!
    @IBOutlet weak var firstNameField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    var isUpdatingResult = false
    var user = User()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isUpdatingResult {
            self.showUserBtn.isHidden = true
            self.registerBtn.setTitle("Update", for: .normal)
            self.emailField.text = self.user.email
            self.firstNameField.text = self.user.firstName
            self.lastNameField.text = self.user.lastName
            self.userNameField.text = self.user.userName
            
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }
    

    @IBAction func showUsersPressed(_ sender: Any) {
        guard let userVC = self.storyboard?.instantiateViewController(withIdentifier: "usersVC") as? UsersVC else {return}
        self.navigationController?.pushViewController(userVC, animated: true)
    }
    @IBAction func resgisterPressed(_ sender: Any) {
        
        
        
        guard let firstName = self.firstNameField.text, firstName != "" else {
            print("first name is empty")
            return
        }
        guard let lastName = self.lastNameField.text, lastName != "" else {
            print("last name is empty")
            return
        }
        guard let userName = self.userNameField.text, userName != "" else {
            print("username is empty")
            return
        }
        guard let email = self.emailField.text, email != "" else {
            print("email is empty")
            return
        }
        
        if isUpdatingResult {
            isUpdatingResult = false
            Store.updateProfile(userId: self.user.id, firstName: firstName, lastName: lastName, userName: userName, email: email) { (success) in
                if success {
                    self.navigationController?.popViewController(animated: true)
                    
                } else {
                    print("user updation failed")
                }
                
            }
            return
        }
        Store.createProfile(firstName: firstName, lastName: lastName, userName: userName, email: email) { (success) in
            if success {
                print("profile created")
                guard let userVC = self.storyboard?.instantiateViewController(withIdentifier: "usersVC") as? UsersVC else {return}
                self.navigationController?.pushViewController(userVC, animated: true)
            }
        }
    }
    

}
