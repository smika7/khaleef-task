//
//  UsersVC.swift
//  KhaleefTask
//
//  Created by Muhammad Iqbal on 01/04/2020.
//  Copyright © 2020 Muhammad Iqbal. All rights reserved.
//

import UIKit

class UsersVC: UIViewController {
    var users: [User] = [User]()
    @IBOutlet weak var userTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        userTableView.delegate = self
        userTableView.dataSource = self
        
        Store.retriveProfile { (userArray) in
            self.users = userArray
            self.configureTableView()
        }
        
        
        Store.observeObjecChanges { (user) in
            self.users.removeAll(where: {
                $0.id == user.id
            })
            self.users.append(user)
            self.configureTableView()
        }
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        
    }
    
    func configureTableView() {
        self.userTableView.rowHeight = UITableView.automaticDimension
        self.userTableView.estimatedRowHeight = 80.0
        self.userTableView.reloadData()
    }
}

extension UsersVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "userCell", for: indexPath) as? UserCell else { return UITableViewCell()}
        cell.user = self.users[indexPath.row]
        
        cell.onUpdateClick = {
            [unowned self] (user) in
            guard let vc = self.storyboard?.instantiateViewController(identifier: "registerVC") as? RegisterUserVC else {return}
            vc.isUpdatingResult = true
            vc.user = user
            self.navigationController?.pushViewController(vc, animated: true)
        }
        cell.onDeleteClick = {
            [unowned self] (user) in
            
            Store.deleteProfile(userId: user.id) { (success) in
                if success {
                    print("deleted successfully")
                    self.users.removeAll(where: {
                        $0.id == user.id
                    })
                    self.configureTableView()
                }
            }
        }
        return cell
    }
    
}


