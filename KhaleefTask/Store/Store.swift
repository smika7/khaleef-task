//
//  Store.swift
//  KhaleefTask
//
//  Created by Muhammad Iqbal on 01/04/2020.
//  Copyright © 2020 Muhammad Iqbal. All rights reserved.
//

import Foundation
import Firebase

class Store: NSObject {
    
  static let rootRef = Database.database().reference()
   
   class func createProfile(firstName: String, lastName: String, userName: String, email: String, completion: @escaping((Bool)->Void)) {
    let id = UUID.init()
    let uid = String(describing: id)
    let userDic = ["firstname": firstName,"lastname": lastName, "username": userName, "email": email, "id": uid]
    rootRef.child("Users").child(uid).setValue(userDic) { (error, reference) in
        
        if let error = error {
            print(error.localizedDescription)
            completion(false)
            return
        } else {
            completion(true)
            
        }
    }
    }
    

    
    
    class func retriveProfile(completion: @escaping(([User])->Void)) {
        var userArray: [User] = [User]()
        let userDB = rootRef.child("Users")
        userDB.observe(.childAdded) { (snapshot) in
            let snapshot = snapshot.value as! Dictionary<String, String>
            let username = snapshot["username"]!
            let firstname = snapshot["firstname"]!
            let lastname = snapshot["lastname"]!
            let email = snapshot["email"]!
            let id = snapshot["id"]!
            let user = User(firstName: firstname, lastName: lastname, userName: username, email: email, id: id)
        
            userArray.append(user)
            completion(userArray)
        }
        
        
       
    }
    
    
    class func observeObjecChanges(completion: @escaping((User)->Void)) {
        
       
        let userDB = rootRef.child("Users")
        userDB.observe(.childChanged) { (snapshot) in
            let snapshot = snapshot.value as! Dictionary<String, String>
            let username = snapshot["username"]!
            let firstname = snapshot["firstname"]!
            let lastname = snapshot["lastname"]!
            let email = snapshot["email"]!
            let id = snapshot["id"]!
            let user = User(firstName: firstname, lastName: lastname, userName: username, email: email, id: id)
            
            
            completion(user)
        }
        
    }
   
    
    
    
   
    class func updateProfile(userId: String,firstName: String, lastName: String, userName: String, email: String, completion: @escaping((Bool)->Void)) {
        
        let userDic = ["firstname": firstName,"lastname": lastName, "username": userName, "email": email, "id": userId]


        rootRef.child("Users").child(userId).updateChildValues(userDic) { (error, reference) in
            if let error = error {
                print(error.localizedDescription)
                completion(false)
            } else {
                completion(true)
            }
        }
        
        
    }
    
    class func deleteProfile(userId: String, completion: @escaping((Bool)->Void)) {
        rootRef.child("Users").child(userId).removeValue()
        completion(true)
    }
}
