//
//  User.swift
//  KhaleefTask
//
//  Created by Muhammad Iqbal on 01/04/2020.
//  Copyright © 2020 Muhammad Iqbal. All rights reserved.
//

import Foundation


struct User {
    
   
     var firstName: String
     var lastName: String
     var email: String
     var userName: String
     var id: String
    init() {
        self.firstName = ""
        self.lastName = ""
        self.userName = ""
        self.email = ""
        self.id = ""
    }
    init(firstName: String, lastName: String, userName: String, email: String, id: String) {
        self.firstName = firstName
        self.lastName = lastName
        self.userName = userName
        self.email = email
        self.id = id
    }
    
    
    
}
